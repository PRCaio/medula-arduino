// INCLUSÃO DE BIBLIOTECAS
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

// DEFINIÇÕES DE PINOS
#define pinRx 10  //informe aqui qual porta Rx está sendo utilizada.
#define pinTx 11  //informe aqui qual porta Tx está sendo utilizada.


#define button 9   //definimos a saída digital 1 com o nome de Button.
#define button2 7   //definimos a saída digital 1 com o nome de Button.
#define button3 6  //definimos a saída digital 2 com o nome de Button2.
#define button4 5
#define button5 12   //definimos a saída digital 1 com o nome de Button.
#define button6 3  //definimos a saída digital 2 com o nome de Button2.
#define button7 2
// DEFINIÇÕES
#define volumeMP3 20  //definimos o volume, entre 0 e 30.

#define DEBUG
// INSTANCIANDO OBJETOS
SoftwareSerial playerMP3Serial(pinRx, pinTx);

DFRobotDFPlayerMini playerMP3;

void setup() {
  pinMode(button, INPUT);   //declaramos Button como INPUT.
  pinMode(button2, INPUT);  //declaramos Button como INPUT.
  pinMode(button3, INPUT);
  pinMode(button4, INPUT);   //declaramos Button como INPUT.
  pinMode(button5, INPUT);  //declaramos Button como INPUT.
  pinMode(button6, INPUT);
  pinMode(button7, INPUT);   //declaramos Button como INPUT.

  Serial.begin(9600);
  playerMP3Serial.begin(9600);

  Serial.println();
  Serial.println(F("Iniciando DFPlayer ... (Espere 3~5 segundos)"));

  if (!playerMP3.begin(playerMP3Serial)) {  // COMUNICAÇÃO REALIZADA VIA SOFTWARE SERIAL
    Serial.println(F("Falha:"));
    Serial.println(F("1.conexões!"));
    Serial.println(F("2.cheque o cartão SD!"));
    while (true) {
      delay(0);
    }
  }

  Serial.println(F("DFPlayer iniciado!"));

  playerMP3.volume(volumeMP3);

#ifdef DEBUG
  Serial.println("o Setup acabou");
#endif
  playerMP3.play(1);
}

void loop() {

  if (digitalRead(button) == HIGH) {
    playerMP3.play(1);
    Serial.println("Tocando pasta 01, musica 001");
    delay(1000);
  }

  if (digitalRead(button2) == HIGH) {
    playerMP3.play(2);
    Serial.println("Tocando pasta 02, musica 002");
    delay(1000);
  }
  
  if (digitalRead(button3) == HIGH) {
    playerMP3.play(3);
    Serial.println("Tocando pasta 03, musica 003");
    delay(1000);
  }

  if (digitalRead(button4) == HIGH) {
    playerMP3.play(4);
    Serial.println("Tocando pasta 04, musica 004");
    delay(1000);
  }
  
  if (digitalRead(button5) == HIGH) {
    playerMP3.play(5);
    Serial.println("Tocando pasta 05, musica 005");
    delay(1000);
  }

  if (digitalRead(button6) == HIGH) {
    playerMP3.play(6);
    Serial.println("Tocando pasta 06, musica 006");
    delay(1000);
  }
  
  if (digitalRead(button7) == HIGH) {
    playerMP3.play(7);
    Serial.println("Tocando pasta 07, musica 007");
    delay(1000);
  }

}